package br.edu.up;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;

public class ExecutorSP {

	public static void main(String[] args) throws Exception {
		Connection conn = getMySqlConnection();
		//String procedimento = "{ call atualizar(0, 0.5) }";
		//CallableStatement call = conn.prepareCall(procedimento);
		CallableStatement call = conn.prepareCall("SELECT * FROM livraria.listatemdelivros;");
		ResultSet rs = call.executeQuery();
		while (rs.next()) {
			System.out.println("Id: " + rs.getString("id"));
			System.out.println("Nome: " + rs.getString("nome"));
			System.out.println("Pre�o: " + rs.getString("preco"));
			System.out.println("-----------");
		}
		conn.close();
	}

	public static Connection getMySqlConnection() throws Exception {
		String driver = "org.gjt.mm.mysql.Driver";
		String url = "jdbc:mysql://localhost/livraria";
		String username = "root";
		String password = "master";

		Class.forName(driver);
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}

//	private static Connection getHSQLConnection() throws Exception {
//		Class.forName("org.hsqldb.jdbcDriver");
//		System.out.println("Driver Loaded.");
//		String url = "jdbc:hsqldb:data/tutorial";
//		return DriverManager.getConnection(url, "sa", "");
//	}

	public static Connection getOracleConnection() throws Exception {
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url = "jdbc:oracle:thin:@localhost:1521:caspian";
		String username = "mp";
		String password = "mp2";

		Class.forName(driver); // load Oracle driver
		Connection conn = DriverManager.getConnection(url, username, password);
		return conn;
	}
}
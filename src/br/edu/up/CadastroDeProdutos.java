package br.edu.up;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CadastroDeProdutos {
	
	public static void main(String[] args) {
		

		Produto p = new Produto();
		p.setNome("Teste 1");
		p.setPreco(new BigDecimal("102.40"));
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("produtos");
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();    
		em.persist(p);
		em.getTransaction().commit();  

		System.out.println("ID da tarefa: " + p.getId());

		em.close();
		
	}
}